package com.medavox.mydiff

import java.io.File
import java.io.RandomAccessFile

/**MyDiff is a diff program optimised for very large binary files
 -- that is, files larger than the available RAM of most modern machines.
 Differences between files are described using insert, delete and transpose operations.
 Unlike other popular binary diff formats,
 the files do not need to be entirely read into memory in order to perform the diff.

 No part of the two files being differenced or the resulting output reside in memory
 during any part of the operation.

 This vastly reduces the memory requirements of this algorithm,
 probably at a mild processing cost.
 But processors are much faster than RAM is large these days anyway.
 */
class MyDiff {
    private val BUFFER_SIZE = 16 * 1024 * 1024//16MiB
    /**Minimum number of significant bytes that are worth recording the equality of.
     * */
    private val MIN_SIG_BYTES = 16L

    /*REMEMBER: we're picking each offset that matches the most first,
    * but we ARE going to use multiple offsets.
    * We're not expecting any one offset to fully match.
    * Identical files should have been filtered out before we invoke this algorithm*/

    /*Prioritised offsets we should check first:
    * 0 offset (file starts are aligned)
    * offset where file ends are aligned
    * ?possibly? all the offsets between these two*/
    //todo: prioritise offsets which are likely to produce more matches

    /*Big-O of algorithm:
    ---------------------
    For a minimum overlap z, how many offsets are there for 2 files of lengths m and n?
    (offsets can be negative)

    answer:
    (m - (z-1) ) + ( n - z )

    or, simplified by aimee:
    m + n - 2z+1

    So this means our Order is roughly O(m+n), or linear. That's good!
    */

    /**Generate a KitchenSink Diff of the differences between the two files.
     * Compare all of A to B at every valid offset.
     * finds the longest runs of bytes in A that exactly match runs in B.
     *
     * prints out in order those which are long enough to bother with.
     * @param a the compared [File] representing the 'before' state
     * @param b the compared [File] representing the 'after' state
     * @param outputFile the file to write the generated diff to
     */
    internal fun kitchenSinkDiff(a:File, b:File, outputFile: File) {
        val allSubsequencesForEveryOffset:MutableSet<OffsetSubsequence> = mutableSetOf()
        //step 1: find all the matching subsequences of A and B, for any offset between them
        val randomA = RandomAccessFile(a, "r")
        val randomB = RandomAccessFile(b, "r")
        //keep track of runs in B that aren't matched anywhere.
        //Every time we match a new run in B to a run in A, subtract that from it.
        val unmatchedAnywhereInB = RangesTracker(b.length(), true)

        //For every offset, record each continuous stretch of identical bytes in the two files.
        for(offset in 0 until Math.max(a.length(), b.length())) {
            //val m = findIdenticalSubsequencesGivenOffset(a, b, offset)
            //allSubsequencesForEveryOffset.add(OffsetSubsequence(m, offset))

            //val highestIndexToCheck = getHighestIndexToCheck(a.length(), b.length(), offset)

            var index = 0L
            //for every position in the overlap between the two files
            // (the overlap is the length of the shorter file)
            var currentEquality = randomA.readUnsignedByte() == randomB.readUnsignedByte()
            randomA.seek((index+offset) % a.length())
            randomB.seek(index)
            //go thru the whole file
            var startOfRun:Long = index
            while(index < Math.min(a.length(), b.length())) {//for every index in the shorter file
                index++
                randomA.seek((index+offset) % a.length())
                randomB.seek(index)

                if((randomA.readUnsignedByte() == randomB.readUnsignedByte()) != currentEquality) {
                    //a change -- either:
                    if(currentEquality //1 the last byte-pair were equal, and these two aren't
                            && index-startOfRun > MIN_SIG_BYTES) {//if this run is long enough
                        //record the end of this run of equal or unequal bytes
                        allSubsequencesForEveryOffset.add(
                                OffsetSubsequence(offset, startOfRun until index))
                        //also remove this matched sequence from the list of regions in B
                        // that have no matches anywhere in A
                        unmatchedAnywhereInB.removeRange(startOfRun until index)
                    } else {//2 the last byte-pair were unequal, and these two are
                        //this is the end of a run of unequal bytes
                    }
                    currentEquality = !currentEquality
                    startOfRun = index
                    //todo: maybe record this dissimilar sequence somehow?
                }//else they are the same (continuing to be equal or unequal), so continue
            }
        }

        //remove uselessly short matching sequences
        //val significantLengthMatches = allSubsequencesForEveryOffset.filter {
            //it.range. >= MIN_SIG_BYTES }

        //step 2: account for every byte of B in the editscript with either
        // 1) a start:end range of bytes that can be copied directly from somewhere in A, or
        // 2) new bytes from B, plonked into the editscript

        //For each index in B from start to finish, find the longest sequence in A to fill in the editscript.
        //Or if there isn't one, fill in original data from B
        var currentIndexInB = 0L
        while(currentIndexInB < b.length()) {
            val indexIsUnmatchedInB:Boolean = unmatchedAnywhereInB.sequences.any {
                currentIndexInB in it }
            if(indexIsUnmatchedInB) {//there are no matching sequences in A
                //copy directly from B, and advance the index pointer to the next matching byte

            }else {//there is a match somewhere in A
                //find the longest match out of all offsets
                val longestMatchInA = allSubsequencesForEveryOffset.
                    filter { currentIndexInB in it.range }.
                    //Don't just check sequences that start on our current index.
                    //also check all runs that start after the current index, and finish after it.
                    //only their remaining length should count towards choosing
                    sortedBy { it.range.endInclusive - currentIndexInB }.last()

                //write a reference to the diff-file, of the start:end in B, and offset in A
                //todo: if we'regoing to use protobufs for  serialisation,
                // we should create  a java object directly representing the protobuf structures.
                // These are probably provided/autogenerated by protobuf
            }

            currentIndexInB++
        }
    }

    fun getLowestValidOffset(aLength:Long, bLength:Long, offset:Long):Long{
        val ret = Math.abs(Math.min(0, offset))
        if(ret+offset >= aLength || ret+offset >= bLength) {
            throw NoOverlapForOffsetException(aLength, bLength, offset)
        }
        return ret+MIN_SIG_BYTES
    }

    fun getHighestValidOffset(aLength: Long, bLength: Long, offset: Long):Long {
        val ret = Math.min(aLength-offset, bLength-1)
        if(ret < 0) {
            throw NoOverlapForOffsetException(aLength, bLength, offset)
        }
        return ret-MIN_SIG_BYTES
    }


    /*We should only track accounted-for sequences in B
    * -------------------------------------------------
    * Sequences in A that have been moved in B can never be fully ruled out of further searches.
    * they might appear more than once in B.
    *
    * In fact EVERY sequence in A may match 0 or more times.
    * Everything may have been deleted from A to B, or there may be no bytes in common at all.
    * */

    /*
        offset is positive;
            compare b[n] with a[n+offset]
            valid indices:
            start=abs(min(0, offset))?
            end=min(aLength-offset, bLength-1)

overlap start: the lowest n where n >= 0 && n+offset >= 0
overlap end: the highest n where n < b.length && n < a.length && n+offset < a.length && n+offset < b.length

            AAAAAAAAAAAA
                   BBBBBBBBBBBB

            start=abs(min(0, offset))?
            =abs(min(0, 7))
            =0
             length of A: 11
             length of B: 12
             offset: 7
             valid indices (from B's perspective, without the offset applied): 0 to 4
             checkable overlap = 5

             or
              AAAAAAAAAAAA
                   BBBB

             length of A: 12
             length of B: 4
             offset: 5
             valid indices (from B's perspective, without the offset applied): 0 to 3
             checkable overlap = 4

             or even
             AAAAA
                    BBBBB
             checkable overlap = 0
             length of A: 5
             length of B: 5
             offset=7
check startOverlap+offset < aLength

        offset is negative;
            compare b[n] with a[n+offset]
            start=abs(min(0, offset))?
            =ab(min(-7, 0))
            =abs(-7)
            = 7
            end=min(aLength-offset, bLength-1)
            =min(12- -7 , 13-1)
            =min(12+7 , 13-1)
            =min(19 , 12)

            valid indices: Math.abs(overlap) to Math.min(b.size()-1, a.size-offset)

                   AAAAAAAAAAAA
            BBBBBBBBBBBBB

            length of a: 12
            length of b: 13
            checkable overlap = 6
            offset= -7
            valid indices: 7 to b.size()-1 = 12

            or
                    AAAAA
            BBBBBBBBBBBBBB

            length of a: 5
            length of b: 14
            offset = -8
            valid indices: 8 to 12

            checkable overlap  = 5

            or
                    AAAAAAA
            BBBBBB

            checkable overlap = 0
            offset= -8
            length of a: 7
            length of b: 6


            equivalent to swapping A and B, and doing Math.abs() on the offset.

            eg a[i] == b[i+offset], where offset = -3
             is the same as
            a[i+offset] == b[i], where offset = 3
            * */
    //simply reverse the positions, and the polarity

    /*offset is 0
            * compare both arrays from their starting positions*/
}
