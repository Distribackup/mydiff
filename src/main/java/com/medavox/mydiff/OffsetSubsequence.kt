package com.medavox.mydiff

import com.medavox.util.validate.Validator

/**Represents a sequence of identical bytes between two files,
 * given the specified offset of B from A.
 *
 * we record each match sequence as its starting offset in each file, its end,
 * and the offset at which it occurs.
 *
 * @property offset The offset which which the start and end/length applies to*/
//the bytes from START to END in B are identical to that range of bytes in A, offset by OFFSET
//so for OffsetSubsequence (0, 11, 4),
//bytes 0-11 in B (inclusive, so 12 bytes) are identical to bytes 4-15 in A
data class OffsetSubsequence(@JvmField val offset: Long,
                             @JvmField val range: LongRange) {
    @JvmField
    val length:Long
    init {
        Validator.check(range.start >= 0,
                IllegalArgumentException("start must be >= 0. Passed range: " + range))
        Validator.check(range.start < range.endInclusive,
                IllegalArgumentException("start must be before end. Passed range: $range"))
        length = range.endInclusive - range.start
    }
}
/*for each offset:long,
 * a list of every matching subsequence:
 *      the start in A,
 *      (the start in B is derived from the start in A + offset)
 *      and the length of the matching sequence*/