package com.medavox.mydiff
/**
The amount of memory this object uses is proportional to the number of discrete sequences it contains:
an empty file uses the least memory (0 sequences),
followed by a completely full sequence run (1 sequence, from start to finish)
I need this for recording UNmatched-anywhere sequences in B
*/
class RangesTracker(val length:Long, initialState:Boolean=false) {
    private val ranges:MutableSet<LongRange> = mutableSetOf()

    val sequences:List<LongRange> get() = ranges.sortedBy { it.start }
    init {
        if(initialState) {
            ranges.add(0L until length)
        }
    }

    /**Set the booleans described by the range as being On, Added or similar.
     * If the specified range overlaps any existing ones,
     * then all overlapping ranges are merged into one larger one.
     * (any indices that are already on will remain on).*/
    fun addRange(newRange:LongRange) {
        //find all ranges touching or overlapping this new one
        val overlappedExistingRanges:List<LongRange> = ranges.filter {
            it.start >= newRange.start || it.endInclusive >= newRange.start-1 ||
                it.endInclusive <= newRange.endInclusive || it.start <= newRange.endInclusive+1
        }

        val includingNewOne:List<LongRange> = overlappedExistingRanges + listOf(newRange)

        //find the new extents of the merged range, formed from all our overlapping chappies
        ranges.removeAll(includingNewOne)
        val newMergedRange:LongRange = includingNewOne.minBy{it.start}!!.start ..
                includingNewOne.maxBy { it.endInclusive }!!.endInclusive

        ranges.add(newMergedRange)
    }

    /**Set the booleans described range as being 'removed', 'off', or similar.
     * Any values already off are left the same; any values that were on before are removed,
     * and the affected range(s) are reduced accordingly.*/
    fun removeRange(rangeToRemove:LongRange) {
        //find all ranges overlapping the one to be deleted
        val overlappedExistingRanges:List<LongRange> = ranges.filter {
            it.start >= rangeToRemove.start || it.endInclusive >= rangeToRemove.start ||
                    it.endInclusive <= rangeToRemove.endInclusive || it.start <= rangeToRemove.endInclusive
        }
        for(range in overlappedExistingRanges) {
            if(range.start >= rangeToRemove.start ) {
                if(range.endInclusive <= rangeToRemove.endInclusive) {
                    //this range is completely inside the range to delete, so delete it completely
                    ranges.remove(range)
                }
                else{
                    //the range to delete overlaps the earlier part of this range
                    //shrink this range: move its start to just after the rangeToDelete's end
                    ranges.remove(range)
                    ranges.add(rangeToRemove.endInclusive+1 .. range.endInclusive)
                }
            }else if(range.endInclusive <= rangeToRemove.endInclusive) {
                //the range to delete overlaps the later part of this range
                //shrink this range: move its end to just before the rangeToDelete's start
                ranges.remove(range)
                ranges.add(range.start .. rangeToRemove.start-1)
            }
        }
    }
}