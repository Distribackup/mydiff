Review of Serialisation Libraries/Technologies/Strategies
========================================================

JSON
----

* Con: Bulky storage format. Not desirable in a diff format for >RAM-size files
* Pro: Very popular - mature ecosystem, many implementations for Java

Cap'n Proto
--------

* Pro: Fast, simple etc. Read the page.
* Con: Mainly for C++; Java implementation still uses C++
* Con: obscure, not very popular

Protocol Buffers
------

* Pro: Most popular custom serialiser by far
    - main repo has 34K stars on GitHub
    - square/wire has 2.7K stars
* Con: weirdly fragmented
    - Choice between google's implementation and Square's Wire
    - Protobufs 3 is recommended, but there's only documentation for 2
* Con: must write message specs - another language syntax to learn
* Con: Requires use of a maven plugin - but we use gradle in this project

Flatbuffers
------

* Pro: pretty popular - main repo has 12K stars on GitHub

MessagePack
----------

* Pro: Up-and-comer
* Pro: good range of language implementations
* Pro: Pure-JVM implementation 
* Con: [binary data is limited to 2GiB](
  https://github.com/msgpack/msgpack/blob/master/spec.md#limitation),
  and 'integers' can only be 64-bit unsigned
* therefore, unsuitable

UBJSON
------

* Pro: compact
* Pro: partially self-documenting
    - fields include type and tag name
* Simple, understandable spec
* arrays have no max size
* support for Longs
    - though only signed Longs. But what's one power-of-2 out of 64?
* support for arbitrary-precision integers AND floats
* 1:1 interoperability with JSON 

Apache Thrift
------------

* Con: very heavyweight
    - major learning overhead?
    - runtime performance cost?
* Con: old code?
    - after all, Apache projects are often abandoned ex-commercial software. Transfer implies obsoletion