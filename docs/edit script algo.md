Edit Script Format
================

Because we additionally use a 'copy' command along with the traditional add/delete commands
in our diff, and because we can process binary data, we need a custom edit-script format.

Given 
1. an original file A,
2. a changed file B, and
3. the diff between them,

Thre are two ways to approach applying our editscript: 

* Creating a new copy of B, using A and the edit-script, or
* Modifying A in-place, turning it into B.

Creating B from A
======

instructions section
--------

Either of the following, zero or more times:

* copy from A (start:end indices)
* copy from B/original content (start:end indices, referring to 

Then followed by END-INSTRUCTIONS-SECTION, aka BEGIN-DATA-SECTION.

Data Section
------------

What then follows is all of the original binary data from B,
concatened together into one long ream of raw data. 

Chunks are copied from here using the indices, which refer to absolute positions in this file
(so no data position will ever be 0, because the instructions section is there).

turning A into B in-place
===================

More difficult; but preferable, as

1. this is what diff does, so is the expected behaviour, and
2. will use less disk space during conversion.

Any of the following, zero or more times:

* sections which remain the same in A and B are left unmentioned.
* sections which exist in A but not in B are deleted. Delete (start:end)
* original sections are reproduced verbatim from B into the edit script, 
  or perhaps referred to using the data-footer idea above.
* sections to be copied from another part of A are more difficult, 
  as we may be copying a section in A to a later part of the file, 
  after it may have been deleted in its original location.
  
  How do we copy from a section we've already deleted 
  (assuming a single-pass over both the file and the edit-script)?

  performing a single-pass over both A and the editscript is a functional requirement,
  because both A and the edit-script may be very large files,
  and only having to read a section of the files into memory at a time is a performance requirement.
  
  Copying any sections-to-be-copied into memory as temporary storage is not a good idea either,
  because again these sections may be very large and/or numerous.

Reversibility
=====

In order to be able to navigate through the archive edition history both backwards and forwards,
our editscript needs to be undoable. That is, given B and the editscript that converted A into B,
we should be able to convert B back into A by unapplying the editscript. 

TODO 27May2019: we should try to de-duplicate new byte literal chunks (or subsets of the same bytes) from B,
by also using all the already-specified literal sections from B as a source for later new sections.

That is, new sections in B may be copied out as a new literal,
or by using already-copied sections from earlier in the file (if any match)