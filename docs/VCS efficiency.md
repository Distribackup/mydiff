from <https://superuser.com/a/159338>:
"
Objects in a pack file can be stored either as plain compressed data 
(same as a loose object, just bundled up with others objects), 
or as compressed deltas against some other object. 

Deltas can be chained together to configurable depths (pack.depth)
and are can be made against any suitable object 
(pack.window controls how widely Git searches for the best delta base; 
a version of an historically unrelated file can be used as a base 
if doing so would yield a good delta compression). 
The latitude that the depth and window size configurations give the delta compression engine 
often result in a better delta compression than the CVS-style 
simple one-version-against-the-next/previous-version “diff” compression.

It is this aggressive delta compression (combined with normal zlib compression) 
that can often let a Git repository (with full history and an uncompressed working tree) 
take less space than a single SVN checkout (with uncompressed working tree and pristine copy).
"
This means that the shortest diff for a new version of a file, might not be the diff against the previous version.
So we need to check if a diff against other files produces a smaller diff.