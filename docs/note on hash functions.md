We do actually need to use a cryptographic hash, 
to prevent input crafting producing predictable outputs.
This is the main difference between cryptographic and non-cryptographic hashes.
Cryptographic hash functions are designed so that it's computationally infeasible to reverse digest in any way,
*and therefore* craft the input to produce a desired hash output.
So we may as well use the strongest one