Rather than creating a list of the differences between the whole of both files,
for EVERY offset,
we could instead limit our search to the sections of B that haven't yet been matched

This approach may not always find the best match, but rather the first. But it would be faster.
This would require us to be able to choose arbitrary subsequences of B to compare against A

DECISION: try this later, after finishing the kitchen-sink approach
(laying out identicality data for ALL offsets, before choosing which subsequences to use
in constructing B from A)

This algorithm is called Toes-First, as it feels out the best path as it goes
The other is called Kitchen-Sink, as it compares all offsets before making any decisions