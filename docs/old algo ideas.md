OLDER ALGO IDEA
(this predates the kitchen-sink diff, but may still be useful)
Once we can no longer find any runs, or all bytes are accounted for,
then we generate the edit script using the record of longest runs,
and include any remaining literal differences
(insertions/deletions: sequences unique to either file).
then, starting from the end of both files going backwards,
check how far from the end both files are identical

every time we decide on which sequence of bytes at which offset is to be used,
record those bytes involved as 'solved' for the output file, or used up, or whatever.
for every run of checking sequences for a given offset,
make sure we're not diffing bytes which have been counted out already.
store this info for both files, as sequences in each file might get ticked off at different times
