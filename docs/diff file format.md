Diff File-Format
=====

This version is a self-documenting format with as much human-readable plaintext as possible.

A diff is 2 files: 

* an INSTRUCTIONS file
    - plaintext which describes how to construct file B from file A and the data file
* a DATA file
    - a BLOB of all the data unique to B

The two files are compressed together into a single archive. 
Compression algorithm TBD, but probably LZ4, LZO or a faster variant of 7Z's algo

Instructions File
--------

Either of the following, zero or more times:

* old_data
    - copy from A (start:end indices)
* new_data
    - copy from data file (start:end indices) 


Data File
------------

All of the data in B which isn't present in A,
concatenated together into one long ream of raw bytes. 

Chunks are copied from here using the indices, which refer to absolute positions in this file

Hex strings do not have spaces (or any other separator characters) between pairs of hex characters.

Header
=====

Tag name            | size in chars | type          | description 
--------------------|---------------|---------------|-------------
format_name         | TBD="diffname"| UTF-8 String  | Googlable file format name
format_ver          | 1+            | uInt string   | starts at 1. Incremented upon a breaking change to this format
old_file_hash       | 128           | hex string    | SHA-512 hash of existing file
old_file_length     | 1+            | uLong string  | nominally an unsigned long (64-bit integer)
new_file_hash       | 128           | hex string    | SHA-512 hash of file to be constructed
new_file_length     | 1+            | uLong string  | nominally an unsigned long (64-bit integer)
data_file_hash      | 128           | hex string    | SHA-512 hash of included data file
data_file_length    | 1+            | uLong string  | nominally an unsigned long (64-bit integer)

Chunk: New Content
====

identifier: new_data

Tag name   |Size (chars)| type  | description 
-----------|-----|--------------|-------------
hash       | 128 | hex string   | SHA-512 hash of the bytes to copy from the data file
start_incl | 1+  | uLong string | start index (inclusive) of the bytes to copy from the data file
end_incl   | 1+  | uLong string | end index (inclusive) of the bytes to copy from the data file

Chunk: Copy from A
=======

identifier: old_data

Tag name   |Size (chars)| type    | description 
-----------|------|------------|------
hash       | 128  | hex string | SHA-512 hash of the bytes to copy from A
start_incl | 1+   | uLong      | start index (inclusive) of the bytes to copy from file A
end_incl   | 1+   | uLong      | end index (inclusive) of the bytes to copy from file A